Opt("MustDeclareVars", 1)
#include<winhttp.au3>
#include<array.au3>
#include<Date.au3>
Global $sReferrer, $sUsername, $sPasswort, $iNextRuntime, $hSession, $hConnection, $iLog

$sUsername = IniRead("Settings.ini", "Login", "Username", "")
If $sUsername = "" Then IniWrite("Settings.ini", "Login", "Username", "DEIN_NAME")
$sPasswort = IniRead("Settings.ini", "Login", "Passwort", "")
If $sPasswort = "" Then IniWrite("Settings.ini", "Login", "Passwort", "DEIN_PASSWORT")
$iNextRuntime = IniRead("Settings.ini", "Data", "NextRuntime", "")
If $iNextRuntime = "" Then IniWrite("Settings.ini", "Data", "NextRuntime", _NowCalc())
$iLog = IniRead("Settings.ini", "Data", "Log", "")
If $iLog = "" Then
	IniWrite("Settings.ini", "Data", "Log", 0)
	$iLog = 0
EndIf
If $sUsername = "" Or $sPasswort = "" Then Exit MsgBox(64, "Fehler", "Trage deine Daten in die Settings.ini ein!")

Func _Warteschlange_setNextRuntime()
	$iNextRuntime = _DateAdd("n", 15, _NowCalc())
	IniWrite("Settings.ini", "Data", "NextRuntime", $iNextRuntime)
EndFunc   ;==>_Warteschlange_setNextRuntime

Func _Warteschlange_warte()
	Local $iSleep
	$iSleep = _DateDiff("s", _NowCalc(), $iNextRuntime) * 1000
	__Log("Warte " & $iSleep / 1000 & " Sekunden bis zum n�chsten Durchgang.")
	Sleep($iSleep)
EndFunc   ;==>_Warteschlange_warte



Func _Warteschlange_herausfordern($iWahl1 = Default, $iWahl2 = Default, $iWahl3 = Default)
	Local $sHTML, $aData[7][2], $aStep, $aAction, $aMac, $aButton, $aList, $iMeinPlatz, $aMeinPlatz
	If $iWahl1 = Default Then $iWahl1 = Random(1, 3, 1)
	If $iWahl2 = Default Then $iWahl2 = Random(1, 3, 1)
	If $iWahl3 = Default Then $iWahl3 = Random(1, 3, 1)

	$sHTML = _WinhttpSimpleRequest($hConnection, "GET", "/herausforderung.html", $sReferrer)
	$aMeinPlatz = StringRegExp($sHTML, '\Qauf Position <b>\E(\d+)\Q</b>\E', 3)
	If Not IsArray($aMeinPlatz) Then
		__Log("FEHLER: KONNTE DEN EIGENEN PLATZ NICHT AUSLESEN")
		Return 0
	EndIf
	$iMeinPlatz = $aMeinPlatz[0]
	__Log("Ich bin auf dem Platz: " & $iMeinPlatz)
	If StringInStr($sHTML, " gegen DICH!") Then
		__Log("Eine Herausforderung liegt vor.")
		$aData[0][0] = "sval1"
		$aData[0][1] = 4;$iWahl1
		$aData[1][0] = "sval2"
		$aData[1][1] = 4;$iWahl2
		$aData[2][0] = "sval3"
		$aData[2][1] = 4;$iWahl3
		$aStep = StringRegExp($sHTML, '\Qname="step" type="hidden" value="\E(\d+?)\Q"\E', 3)
		$aData[3][0] = "step"
		$aData[3][1] = $aStep[0]
		$aAction = StringRegExp($sHTML, '\Qname="action" type="hidden" value="\E(\d+?)\Q"\E', 3)
		$aData[4][0] = "action"
		$aData[4][1] = $aAction[0]
		$aMac = StringRegExp($sHTML, '\Qname="mac" type="hidden" value="\E(\d+?)\Q"\E', 3)
		$aData[5][0] = "mac"
		$aData[5][1] = $aMac[0]
		$aButton = StringRegExp($sHTML, '\Qtype="submit" name="button" id="button" value="\E(.+?)\Q"\E', 3)
		$aData[6][0] = "button"
		$aData[6][1] = $aButton[0]
		$sHTML = _WinhttpSimpleRequest($hConnection, "POST", "/indexx.php?", $sReferrer, __CreatePOST($aData), "Content-Type: application/x-www-form-urlencoded")
		If StringInStr($sHTML, "Die erste Runde: Los!") Then
			__Log("Vorliegende Herausforderung wurde erledigt")
		Else
			__Log("FEHLER: VORLIEGENDE HERAUSFORDERRUNG KONNT NICHT BEARBEITET WERDEN!")
			Return 0
		EndIf
	EndIf
	$sHTML = _WinhttpSimpleRequest($hConnection, "GET", "/schlange.html", $sReferrer)
	__Log("Lese die Spieler aus!")
	$aList = StringRegExp($sHTML, '\Quser[\E(\d+)\Q] = new Array(\E\d+,\d+,\d+,".+?",(\d+),\d+,0', 3)
	If Not IsArray($aList) Then
		__Log("FEHLER: DIE SPIELERLISTE KONNT NICHT AUSGELESEN WERDEN")
		Return 0
	EndIf
	For $i = 1 To UBound($aList) - 1 Step 2
		If $aList[$i - 1] >= $iMeinPlatz Then
			__Log("Keine Personen vor mit sind frei.")
			Return 0
		EndIf
		$sHTML = _WinhttpSimpleRequest($hConnection, "GET", "/herausforderung.html?id=" & $aList[$i], $sReferrer)
		If StringInStr($sHTML, "Du bist zu ungeduldig ") Then
			__Log("Maximale Anzahl von Herausforderungen erreicht.")
			Return 0
		ElseIf StringInStr($sHTML, "Gegen diesen Jemand") Then
			__Log("Gegen den Spieler mit der ID " & $aList[$i] & " wurde schon eine herausforderung gestellt.")
			ContinueLoop
		ElseIf StringInStr($sHTML, "DU gegen ") Then
			__Log("Fordere den Spieler mit der ID " & $aList[$i] & " heraus")
			$aData[0][0] = "sval1"
			$aData[0][1] = $iWahl1
			$aData[1][0] = "sval2"
			$aData[1][1] = $iWahl2
			$aData[2][0] = "sval3"
			$aData[2][1] = $iWahl3
			$aStep = StringRegExp($sHTML, '\Qname="step" type="hidden" value="\E(\d+?)\Q"\E', 3)
			$aData[3][0] = "step"
			$aData[3][1] = $aStep[0]
			$aAction = StringRegExp($sHTML, '\Qname="action" type="hidden" value="\E(\d+?)\Q"\E', 3)
			$aData[4][0] = "action"
			$aData[4][1] = $aAction[0]
			$aMac = StringRegExp($sHTML, '\Qname="mac" type="hidden" value="\E(\d+?)\Q"\E', 3)
			$aData[5][0] = "mac"
			$aData[5][1] = $aMac[0]
			$aButton = StringRegExp($sHTML, '\Qtype="submit" name="button" id="button" value="\E(.+?)\Q"\E', 3)
			$aData[6][0] = "button"
			$aData[6][1] = $aButton[0]
			$sHTML = _WinhttpSimpleRequest($hConnection, "POST", "/indexx.php?", $sReferrer, __CreatePOST($aData), "Content-Type: application/x-www-form-urlencoded")
			If StringInStr($sHTML, "Okay, du hast deine Wahl getroffen") Then
				ContinueLoop
			EndIf
		Elseif StringInStr($sHTML, 'id="messCont">') Then
			$sHTML=_WinhttpSimpleRequest($hConnection, "GET", "/schlange.html", $sReferrer)
			ContinueLoop
		EndIf
	Next
EndFunc   ;==>_Warteschlange_herausfordern



Func _Warteschlange_Logout()
	Local $sHTML
	$sHTML = _WinhttpSimpleRequest($hConnection, "GET", "/log-out.html", $sReferrer)
	$sHTML = _WinhttpSimpleRequest($hConnection, "GET", "/indexx.php?action=1", $sReferrer)
	_WinhttpCloseHandle($hConnection)
	_WinhttpCloseHandle($hSession)
	__Log("Verbindung zum Server getrennt.")
EndFunc   ;==>_Warteschlange_Logout



Func _Warteschlange_Login()
	__Log("Einloggen!")
	$hSession = _Winhttpopen("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36")
	$hConnection = _WinhttpConnect($hSession, "warteschlange.bademeister.com")
	Local $fReturn, $sHTML, $aData[5][2], $aAction, $aMac, $aSID
	$aData[0][0] = "nickname"
	$aData[0][1] = $sUsername
	$aData[1][0] = "pwd1"
	$aData[1][1] = $sPasswort
	$sHTML = _WinhttpSimpleRequest($hConnection, "GET", "/log-in.html", $sReferrer)
	$aAction = StringRegExp($sHTML, '\Qtype="hidden" name="action" value="\E(\d+?)\Q"\E', 3)
	If Not IsArray($aAction) Then
		__LOG("FEHLER: Action Value KONNTE NICHT AUSGELESEN WERDEN")
		Return 0
	EndIf
	$aData[2][0] = "action"
	$aData[2][1] = $aAction[0]
	$aMac = StringRegExp($sHTML, '\Qtype="hidden" name="mac" value="\E(\d+?)\Q"\E', 3)
	If Not IsArray($aMac) Then
		__LOG("FEHLER: Mac Value KONNTE NICHT AUSGELESEN WERDEN")
		Return 0
	EndIf
	$aData[3][0] = "mac"
	$aData[3][1] = $aMac[0]
	$aSID = StringRegExp($sHTML, '\Qtype="hidden" name="sid" value="\E(\w+?)\Q"\E', 3)
	If Not IsArray($aSID) Then
		__LOG("FEHLER: Sid Value KONNTE NICHT AUSGELESEN WERDEN")
		Return 0
	EndIf
	$aData[4][0] = "sid"
	$aData[4][1] = $aSID[0]
	$sHTML = _WinhttpSimpleRequest($hConnection, "POST", "/indexx.php?", $sReferrer, __CreatePOST($aData), "Content-Type: application/x-www-form-urlencoded")
	If StringInStr($sHTML, "Eingeloggt als") Then
		__Log("Anmeldung erfolgreich!")
		Return 1
	Else
		__Log("Anmeldung fehlgeschlagen!")
		Return 0
	EndIf
EndFunc   ;==>_Warteschlange_Login


Func __Debug($sString)
	Local $sFileName = "Debug.html", $hFile
	$hFile = FileOpen($sFileName, 2)
	FileWrite($hFile, $sString)
	FileClose($hFile)
EndFunc   ;==>__Debug


Func __CreatePOST($aData)
	Local $sReturn = ""
	For $i = 0 To UBound($aData) - 1
		$sReturn &= StringFormat("%s=%s&", $aData[$i][0], __WinHttpURLEncode($aData[$i][1]))
	Next
	$sReturn = StringTrimRight($sReturn, 1)
	Return $sReturn
	;[0]	Name|Wert
	;[1]	Name|Wert
EndFunc   ;==>__CreatePOST

Func __Log($sString)
	If $iLog = 1 Then FileWrite("Log.txt", StringFormat("%s|\t\t\t%s\n", _Now(), $sString))
EndFunc   ;==>__Log




